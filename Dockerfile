FROM docker.io/library/python:3.7-alpine

RUN apk add --update --no-cache \
    openssh-client \
    rsync \
    musl-dev \
    libffi-dev \
    openssl-dev \
    cargo
    
RUN apk add --update --no-cache \
    --virtual .build-deps \
    make \
    gcc \
    python3-dev \
    && pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir --user ansible \
    && pip install --no-cache-dir --user ansible-core \
    && apk del .build-deps



